import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ImageBackground, ScrollView, BackHandler, Alert, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Carte from './source/components/elements/Carte';
import Header from './source/components/section/Header';
import Fleche from './source/components/elements/Fleche';
import ProfileScreen from './source/screen/Profil';
import GameScreen from './source/screen/Game';
import RegleScreen from './source/screen/Regle';
import GameOverScreen from './source/screen/GameOver';
import ScoreScreen from './source/screen/Score';
import Batteries from './source/components/elements/Batteries';


//////////////////////////////////////////////////////////////////////////
///////////////////////////// PAGE D'ACCEUIL /////////////////////////////
//////////////////////////////////////////////////////////////////////////

function HomeScreen({ navigation }) {
  const [img, setImg] = useState(null);
  useEffect(() => {
    const backAction = () => {
      Alert.alert('Attention !', 'Êtes vous sur de vouloir retourner à l\'écran d\'acceuil.', [
        {
          text: 'NON',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'OUI', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);


  const updateBatterie= (Batterie) => {
    console.log('Batterie : ' + Batterie + '%');
    if (Batterie > 80) {
      console.log('Battery > 80%');
      setImg(require('./assets/img/img_niv/1.jpg'));
    } else if (Batterie > 60) {
      console.log('Battery > 60%');
      setImg(require('./assets/img/img_niv/2.jpg'));
    } else if (Batterie > 40) {
      console.log('Battery > 40%');
      setImg(require('./assets/img/img_niv/3.jpg'));
    } else if (Batterie > 20) {
      console.log('Battery > 20%');
      setImg(require('./assets/img/img_niv/4.jpg'));
    } else {
      console.log('Battery < 20%');
      setImg(require('./assets/img/img_niv/5.jpg'));
    }
  };

  const cartes = [
    {
      titre: 'Régle du jeu',
      texte: 'Comment jouer ? \nComment sauvegarder son score ?',
      imageSource:  require('./assets/img/regle.png'),
      OnPress:"Regle",
      bouton_contenue :'Accédez',
    },
    {
      titre: 'React-ion',
      texte: 'Jouez au mode classic de React-ion !',
      imageSource: require('./assets/img/game.png'),
      OnPress:"Game",
      bouton_contenue :'Play',
    },
    {
      titre: 'Score',
      texte: 'Consultez vos scores !',
      imageSource:  require('./assets/img/score.png'),
      OnPress:"Score",
      bouton_contenue :'Accédez',
    },
    {
      titre: 'Profil',
      texte: 'Consultez votre profil !',
      imageSource: require('./assets/img/user.png'),
      OnPress:"Profile",
      bouton_contenue :'Accédez',
    },
  ];
  
  return (
    <View style={styles.home}>
      <ImageBackground source={img} resizeMode="cover" style={styles.imageBack}>
        <Batteries updateBatterie={updateBatterie} />
        <ScrollView>
          {cartes.map((carte) => (
            <Carte
            titre={carte.titre}
            texte={carte.texte}
            imageSource={carte.imageSource}
            onPress={() => navigation.navigate(carte.OnPress)}
            bouton_contenue={carte.bouton_contenue}
            />
            ))}
        </ScrollView>
        <Fleche/>
        </ImageBackground>
    </View>
  );
}


const Stack = createNativeStackNavigator();

//////////////////////////////////////////////////////////////////////////
///////////////////////////// GESTION DE NAV /////////////////////////////
//////////////////////////////////////////////////////////////////////////

function App({}) {

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#CC905C',
          },
          headerTintColor: '#000',
          headerRight: () => (
            <Header/>
          )
        }}>

        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'React-ion',
            headerTitleStyle: {
              fontSize: 20,
            },
          }}
        />
        <Stack.Screen
          name="Profile"
          component={ProfileScreen}
          options={{ title: 'Profil' }}
        />
        <Stack.Screen
          name="Game"
          component={GameScreen}
          options={{ title: 'Game' , headerShown: false }}
        />
        <Stack.Screen
          name="Regle"
          component={RegleScreen}
          options={{ title: 'Régle du jeu' }}
        /> 
        <Stack.Screen
          name="GameOver"
          component={GameOverScreen}
          options={{ title: 'GameOver' , headerShown: false  }}
        />
        <Stack.Screen
          name="Score"
          component={ScoreScreen}
          options={{ title: 'Score' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

//////////////////////////////////////////////////////////////////////////
///////////////////////////// GESTION DES STYLES /////////////////////////
//////////////////////////////////////////////////////////////////////////

const styles = StyleSheet.create({
  bouton: {
    backgroundColor: '#926740',
    color: '#fff',
    padding: 20,
    borderRadius: 10,
    margin: 40,
    marginHorizontal: 100,
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
  },

  home:{
    flex: 1, 
    justifyContent: "center",
  },

  imageBack: {
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#2b2017a0",
    margin: 20,
    padding: 20,
    borderRadius: 10,
  },
});

export default App;
