import React, { useState, useEffect } from 'react';
import { View, Text, ImageBackground, StyleSheet, ScrollView } from 'react-native';
import Batteries from '../components/elements/Batteries';

export default function RegleScreen() {
  const [img, setImg] = useState(null);

  const updateBatterie= (Batterie) => {
    console.log('Batterie : ' + Batterie + '%');
    if (Batterie > 80) {
      console.log('Battery > 80%');
      setImg(require('./../../assets/img/img_niv/1.jpg'));
    } else if (Batterie > 60) {
      console.log('Battery > 60%');
      setImg(require('./../../assets/img/img_niv/2.jpg'));
    } else if (Batterie > 40) {
      console.log('Battery > 40%');
      setImg(require('./../../assets/img/img_niv/3.jpg'));
    } else if (Batterie > 20) {
      console.log('Battery > 20%');
      setImg(require('./../../assets/img/img_niv/4.jpg'));
    } else {
      console.log('Battery < 20%');
      setImg(require('./../../assets/img/img_niv/5.jpg'));
    }
  };




  return (
      <View style={styles.container}>
        <ImageBackground source={img} resizeMode="cover" style={styles.imageBack}>
        <ScrollView>
          <Batteries updateBatterie={updateBatterie} />
          <Text style={styles.title}>Comment jouer à React-ion ?</Text>
          <Text style={styles.text}>React-ion est un jeu de réflexe. Quand vous arrivez sur le jeu, une flèche centrale pointera aléatoirement vers 4 boutons. Votre objectif est d'appuyer le plus rapidement possible sur le bouton visé par la flèche centrale. Attention, vous n'aurez qu'une seconde pour appuyer sur le bon bouton. Si vous vous trompez de bouton ou que vous n'arrivez pas à appuyer à temps, alors vous perdez.</Text>
          <Text style={styles.title}>Comment sauvegarder son score ?</Text>
          <Text style={styles.text}>Si vous avez un compte, votre score sera automatiquement sauvegardé. Si vous n'avez pas de compte, vous ne pourrez pas sauvegarder votre score. Mais il vous suffit d'aller sur la page 'Profil' pour vous créer un compte.</Text>
          </ScrollView>
        </ImageBackground>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
    margin: 20,
    padding: 20,
    borderRadius: 10,
  },
  imageBack: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  text: {
    color: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0.8)',
    margin: 20,
    padding: 20,
    borderRadius: 10,
  },
});
