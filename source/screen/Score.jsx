import React, { useState } from 'react';
import { View, Text, TouchableWithoutFeedback, Image, StyleSheet, ImageBackground} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Animatable from 'react-native-animatable';
import Batteries from './../../source/components/elements/Batteries';

export default function ScoreScreen({ navigation }) {

  const [user, setUser] = useState(null);
  const [score, setScore] = useState(null);
  const [img, setImg] = useState(null);

  React.useEffect(() => {
    getDataProfil().then((data) => {
      setUser(data);
    });
  }, []);

  React.useEffect(() => {
    getDataScore().then((data) => {
      setScore(data);
    });
  }, []);

  const getDataProfil = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('profil-key');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e);
    }
  }

  const getDataScore = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('score');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e);
    }
  }

  const refreshProfil = async () => {
    try {
      const data = await getDataProfil();
      setUser(data);
    } catch (e) {
      console.error(e);
    }
  };

  const refreshScore = async () => {
    try {
      const data = await getDataScore();
      setScore(data);
    } catch (e) {
      console.error(e);
    }
  };

  const updateBatterie= (Batterie) => {
    console.log('Batterie : ' + Batterie + '%');
    if (Batterie > 80) {
      console.log('Battery > 80%');
      setImg(require('./../../assets/img/img_niv/1.jpg'));
    } else if (Batterie > 60) {
      console.log('Battery > 60%');
      setImg(require('./../../assets/img/img_niv/2.jpg'));
    } else if (Batterie > 40) {
      console.log('Battery > 40%');
      setImg(require('./../../assets/img/img_niv/3.jpg'));
    } else if (Batterie > 20) {
      console.log('Battery > 20%');
      setImg(require('./../../assets/img/img_niv/4.jpg'));
    } else {
      console.log('Battery < 20%');
      setImg(require('./../../assets/img/img_niv/5.jpg'));
    }
  };

    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Batteries updateBatterie={updateBatterie} />
      <ImageBackground
        source={img}
        style={styles.imageBack}
      >
        {user ? (
          <>
            <Text style={styles.title}>Profil</Text>
            <Text style={styles.text}>Pseudo : {user.username}</Text>
            {score ? (
              <>
                <Text style={styles.text}>Meilleur score : {score.bestScore}</Text>
                <Text style={styles.text}>Dernier Score : {score.lastScore}</Text>
              </>
            ) : (
              <Text style={styles.text}>Vous n'avez pas encore de score enregistré</Text>
            )}
          </>
        ) : (    
          <>
            <Text style={styles.text}>Vous ne pouvez pas conservez vos score et les consultez si vous n'êtes pas connecter</Text>
            <TouchableWithoutFeedback
              onPress={() => navigation.navigate('Profile')}>
              <Text style={styles.bouton}>SE CONNECTER</Text>
            </TouchableWithoutFeedback> 
          </>
        )}
        <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', paddingBottom: 20, paddingRight: 20 }}>
          <Animatable.View
            animation="pulse" easing="ease-out" iterationCount="infinite"
            style={{
              width: 50,
              height: 50,
              padding: 10,
              backgroundColor: 'rgba(255, 255, 255, 0.5)',
              borderRadius: 100,
            }}>
            <TouchableWithoutFeedback 
              onPress={() => {
                refreshProfil();
                refreshScore();
              }}
            >
              <Image
                source={require('./../../assets/img/refresh.png')}
                style={{ width: '100%', height: '100%', resizeMode: 'contain' }}
              />
            </TouchableWithoutFeedback>
          </Animatable.View>
        </View>
      </ImageBackground>
      </View>
    );
  }
  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      color: '#fff',
      fontSize: 25,
      fontWeight: 'bold',
      textAlign: 'center',
      backgroundColor: 'rgba(0,0,0,0.8)',
      margin: 20,
      padding: 20,
      borderRadius: 10,
    },
    imageBack: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
    },
    text: {
      color: '#fff',
      fontSize: 15,
      fontWeight: 'bold',
      textAlign: 'center',
      backgroundColor: 'rgba(0,0,0,0.6)',
      margin: 20,
      padding: 20,
      borderRadius: 10,
    },
    bouton: {
      backgroundColor: '#926740',
      color: '#fff',
      padding: 20,
      borderRadius: 10,
      margin: 40,
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 20,
      fontWeight: 'bold',
    },
  });