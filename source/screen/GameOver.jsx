import React from 'react';
import { View, Text, ImageBackground, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const GameOverScreen = ({ route, navigation }) => {
  const { Score } = route.params;
  console.log('Score final: ' + Score);

  const [user, setUser] = React.useState(null);
  const [score, setScore] = React.useState(null);

  React.useEffect(() => {
    getData().then((data) => {
      setUser(data);
    });
  }, []);

  React.useEffect(() => {
    getDataScore().then((data) => {
      setScore(data);
    });
  }, []);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('profil-key');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e);
    }
  }

  const getDataScore = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('score');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e);
    }
  }

  const storeDataScore = async (value) => {
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem('score', jsonValue);
    } catch (e) {
      console.log(e);
    }
  };

  const addScore = (valueScore) => {
    let NewScore;
    try {
      NewScore = { lastScore: valueScore, bestScore: score.bestScore };
      if (score.bestScore < valueScore) {
        NewScore = { lastScore: valueScore, bestScore: valueScore };
      }
    } catch (e) {
      NewScore = { lastScore: valueScore, bestScore: valueScore };
    }
    console.log(NewScore);
    storeDataScore(NewScore).then(() => {
      setScore(NewScore);
    });
  };

  // addScore();
  return (
    <ImageBackground
      source={require('./../../assets/img/background.jpg')}
      style={styles.imageBack}
    >
      <View style={styles.home}>
        <Text style={styles.title}>GAME OVER</Text>
        {score ? (
          <>
            <Text style={[styles.text, { margin: 5 }]}>Meilleur score : {score.bestScore}</Text>
            <Text style={[styles.text, { margin: 5 }]}>Dernier score : {score.lastScore}</Text>
          </>
        ) : (
          <Text style={[styles.text, { margin: 5 }]}>Vous n'avez pas encore de score enregistré</Text>
        )}
        <Text style={[styles.text, { margin: 5 }]}>Score: {Score}</Text>
        {user ? (
          <>
            <Text style={[styles.text, { margin: 5 }]}>Pseudo : {user.username}</Text>
          </>
        ) : () => { return null }}
        {score ? (
          <>  
            <View style={styles.Menu}>
              <TouchableWithoutFeedback
                onPress={() => addScore(Score)}>
                <Text style={[styles.bouton, styles.bouton2]}>Sauvegarder le score</Text>
              </TouchableWithoutFeedback>        
            </View>
          </>
        ) : () => { return null }}

        <View style={styles.Menu}>
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate('Home')}>
            <Text style={styles.bouton}>Rejouer</Text>
          </TouchableWithoutFeedback>        
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  bouton: {
    backgroundColor: '#926740',
    color: '#fff',
    padding: 20,
    borderRadius: 10,
    margin: 40,
    marginHorizontal: 100,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  bouton2: {
    margin: 0,
    marginHorizontal: 0,
    fontSize: 14,
    fontWeight: 'bold',
  },
  title: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0.8)',
    margin: 20,
    padding: 20,
    borderRadius: 10,
  },
  home: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  imageBack: {
    flex: 1,
    resizeMode: 'cover', // ou 'stretch' selon vos préférences
  },

  text: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
    margin: 20,
    padding: 20,
    borderRadius: 10,
  },

  Menu: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default GameOverScreen;
