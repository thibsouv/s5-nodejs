import React, { useEffect, useRef, useState } from 'react';
import { View, Text, ImageBackground, StyleSheet } from 'react-native';
import Arrow from '../components/game/Arrow';

export default function GameScreen ({ navigation }){

  const updateDefaite = (newDefaite) => {
    console.log('Score : ' + newDefaite);
    navigation.navigate('GameOver', { Score: newDefaite });
  };

  console.log('----------------');
  console.log('----------------');
  console.log('Démarrage du jeu');
  console.log('----------------');
  console.log('----------------');

  return (
    <View style={styles.container}>
      <ImageBackground source={require('./../../assets/img/img_niv/1.jpg')} resizeMode="cover" style={styles.imageBack}>
        <Arrow updateDefaite={updateDefaite} />
      </ImageBackground>
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffd282',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  imageBack: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
});

