import React, { useState } from 'react';
import { View, Text, StyleSheet, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Modal from 'react-native-modal';
import LoginForm from './../components/elements/Login';
import Batteries from './../../source/components/elements/Batteries';

export default function ProfileScreen({ navigation }) {
  const [user, setUser] = React.useState(null);
  const [pseudo, setPseudo] = useState('');
  const [email, setEmail] = useState('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [img, setImg] = useState(null);

  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  React.useEffect(() => {
    getData().then((data) => {
      setUser(data);
    });
  }, []);

  const logout = () => {
    storeData(null).then(() => {
      setUser(null);
    });
  };

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('profil-key');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  };

  const storeData = async (value) => {
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem('profil-key', jsonValue);
    } catch (e) {
      console.log(e);
    }
  };

  const handlePseudoChange = (text) => {
    setPseudo(text);
  };

  const handleEmailChange = (text) => {
    setEmail(text);
  };

  const handleLogin = () => {
    if (pseudo != null && email != null) {
      const User = { username: pseudo, email: email };
      storeData(User).then(() => {
        setUser(User);
      });
    }
    toggleModal();
  };

  const updateBatterie= (Batterie) => {
    console.log('Batterie : ' + Batterie + '%');
    if (Batterie > 80) {
      console.log('Battery > 80%');
      setImg(require('./../../assets/img/img_niv/1.jpg'));
    } else if (Batterie > 60) {
      console.log('Battery > 60%');
      setImg(require('./../../assets/img/img_niv/2.jpg'));
    } else if (Batterie > 40) {
      console.log('Battery > 40%');
      setImg(require('./../../assets/img/img_niv/3.jpg'));
    } else if (Batterie > 20) {
      console.log('Battery > 20%');
      setImg(require('./../../assets/img/img_niv/4.jpg'));
    } else {
      console.log('Battery < 20%');
      setImg(require('./../../assets/img/img_niv/5.jpg'));
    }
  };


  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Batteries updateBatterie={updateBatterie} />
      <ImageBackground
        source={img}
        style={styles.imageBack}
      >
        {user ? (
          <>
            <Text style={styles.title}>Profil</Text>
            <Text style={styles.text}>Pseudo : {user.username}</Text>
            <Text style={styles.text}>E-mail : {user.email}</Text>
            <TouchableWithoutFeedback onPress={logout}>
              <Text style={styles.bouton}>DECONNEXION</Text>
            </TouchableWithoutFeedback>
          </>
        ) : (
          <View style={styles.container}>
            <TouchableWithoutFeedback onPress={toggleModal}>
              <Text style={styles.bouton}>CONNEXION</Text>
            </TouchableWithoutFeedback>
            <Modal isVisible={isModalVisible}>
              <View>
                {/* Passer les fonctions de gestion du formulaire comme props */}
                <LoginForm
                  onPseudoChange={handlePseudoChange}
                  onEmailChange={handleEmailChange}
                />
                <View>
                  <TouchableWithoutFeedback onPress={handleLogin}>
                    <Text style={styles.bouton}>VALIDER</Text>
                  </TouchableWithoutFeedback>
                </View>
              </View>
            </Modal>
          </View>
        )}
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      color: '#fff',
      fontSize: 25,
      fontWeight: 'bold',
      textAlign: 'center',
      backgroundColor: 'rgba(0,0,0,0.8)',
      margin: 20,
      padding: 20,
      borderRadius: 10,
    },
    imageBack: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
    },
    text: {
      color: '#fff',
      fontSize: 15,
      fontWeight: 'bold',
      textAlign: 'center',
      backgroundColor: 'rgba(0,0,0,0.6)',
      margin: 20,
      padding: 20,
      borderRadius: 10,
    },
    bouton: {
      backgroundColor: '#926740',
      color: '#fff',
      padding: 20,
      borderRadius: 10,
      margin: 40,
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 20,
      fontWeight: 'bold',
    },
  });