import React, { useState, useEffect } from 'react';
import { View, Text, Image, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Vibration, Platform } from 'react-native';

export default function Arrow({ updateDefaite }) {
  const [randomAngle, setRandomAngle] = useState(Math.floor(Math.random() * 4));
  const [points, setPoints] = useState(0);
  
  function ChangeAngle() {
    setRandomAngle(Math.floor(Math.random() * 4));
  }

  const vibrate = () => {
    if (Platform.OS === 'android' || Platform.OS === 'ios') {
      Vibration.vibrate([1000, 500, 1000]);
    }
  };

  function addPoint() {
    setPoints((prevPoints) => prevPoints + 1);
    console.log(points + 1);
    vibrate();
    imageRef.current.animate({ 0: { scale: 1 }, 0.5: { scale: 1.2 }, 1: { scale: 1 } }, 500);
  }

  function VerifPoint(angle) {
    if (randomAngle === angle) {
      ChangeAngle();
      addPoint();
    } else {
      console.log('perdu');
      updateDefaite(points);
      console.log('points : ' + points);
    }
  }

  const imageRef = React.createRef();
  const animationChrono = React.createRef();

  useEffect(() => {
    const AnimationFin = (result) => {
      if (result.finished && points > 1) {
        console.log('perdu');
        updateDefaite(points);
        console.log('points : ' + points);
      }
    };

    animationChrono.current.animate(
      { 0: { width: '100%', height: '1%' }, 1: { width: '100%', height: '100%' } },1000
    ).then(AnimationFin);
  }, [points]);

  return (
    <View style={styles.container}>
      <Animatable.View ref={imageRef}>
        <Animatable.View
          animation="pulse"
          easing="ease-out"
          iterationCount="infinite"
          style={styles.animation}
        >
          <Image
            source={require('./../../../assets/img/arrow_game.png')}
            style={{ width: '100%', height: '100%', resizeMode: 'contain', transform: [{ rotate: `${randomAngle * 90}deg` }] }}
          />
        </Animatable.View>
      </Animatable.View>
      <TouchableWithoutFeedback onPress={() => VerifPoint(0)}>
        <Text style={[styles.boutonJeu, styles.boutonHaut]}>■</Text>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={() => VerifPoint(1)}>
        <Text style={[styles.boutonJeu, styles.boutonDroite]}>■</Text>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={() => VerifPoint(2)}>
        <Text style={[styles.boutonJeu, styles.boutonBas]}>■</Text>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={() => VerifPoint(3)}>
        <Text style={[styles.boutonJeu, styles.boutonGauche]}>■</Text>
      </TouchableWithoutFeedback>
      <Animatable.View
        ref={animationChrono}
        style={styles.animationChrono}
      >
        <Text style={styles.chrono}></Text>
      </Animatable.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  animation: {
    width: 110,
    height: 110,
    padding: 20,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    borderRadius: 100,
  },
  boutonJeu: {
    width: 60,
    height: 60,
    borderRadius: 100,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    zIndex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    justifyContent: 'center',
    position: 'absolute',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  boutonHaut: {
    top: 0,
    marginTop: 40,
  },
  boutonDroite: {
    left: 150,
  },
  boutonBas: {
    bottom: 0,
    marginBottom: 20,
  },
  boutonGauche: {
    left: -100,
  },
  chrono: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    width: '100%',
    height: '100%',
  },
  animationChrono: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    textAlign: 'center',
    justifyContent: 'center',
    zIndex: -1,
  },
});
