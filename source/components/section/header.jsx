import { Image,View, TouchableWithoutFeedback, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';



export default function Header() {
  const navigation = useNavigation();

  return (
    <View style={styles.Menu}>
    <TouchableWithoutFeedback
      onPress={() => navigation.navigate('Score')}>
      <Image style={[styles.logo,{height: 25,}]} source={require('./../../../assets/img/trophy.png')} />
    </TouchableWithoutFeedback>
    <TouchableWithoutFeedback
      onPress={() => navigation.navigate('Profile')}>
      <Image style={[styles.logo,{width: 27,}]} source={require('./../../../assets/img/profil.png')} />
    </TouchableWithoutFeedback>
  </View>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  Menu: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
