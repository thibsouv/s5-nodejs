import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

const Carte = ({ titre, texte, imageSource, onPress, bouton_contenue }) => {
  return (
    <View style={styles.carteContainer}>
      <Image source={imageSource} style={styles.image} />
      <View style={styles.contenu}>
        <Text style={styles.titre}>{titre}</Text>
        <Text>{texte}</Text>
        <TouchableOpacity onPress={onPress} style={styles.bouton}>
          <Text style={styles.texteBouton}>{bouton_contenue}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  carteContainer: {
    backgroundColor: '#dfa372',
    borderRadius: 10,
    overflow: 'hidden',
    margin: 10,
    elevation: 3,
  },
  image: {
    backgroundColor: '#dfa774',
    height: 200,
    width: '100%',
  },
  contenu: {
    padding: 10,
  },
  titre: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  bouton: {
    backgroundColor: 'rgba(94, 70, 51, 0.753)',
    padding: 10,
    borderRadius: 5,
    marginTop: 10,
  },
  texteBouton: {
    color: '#fff',
    textAlign: 'center',
  },
});

export default Carte;
