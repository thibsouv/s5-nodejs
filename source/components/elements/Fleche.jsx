import * as React from 'react';
import { View, Image } from 'react-native';
import * as Animatable from 'react-native-animatable';

export default function Fleche() {



    return (
        <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', paddingBottom: 20, paddingRight: 20 }}>
            <Animatable.View
                animation="pulse" easing="ease-out" iterationCount="infinite"
                style={{
                    width: 50,
                    height: 50,
                    padding: 10,
                    backgroundColor: 'rgba(255, 255, 255, 0.5)',
                    background: 'linear-gradient(transparent, white)',
                    borderRadius: 100,
                }}
            >
                <Image
                source={require('./../../../assets/img/arrow.png')} 
                style={{ width: '100%', height: '100%', resizeMode: 'contain' }}
                />
            </Animatable.View>
        </View>
    );
}
