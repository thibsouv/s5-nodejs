import React, { useEffect, useState } from 'react';
import * as Battery from 'expo-battery';
import { StyleSheet, Text, View } from 'react-native';

export default function Batteries({ updateBatterie }) {
  const [batteryLevel, setBatteryLevel] = useState(null);

  useEffect(() => {
    const _subscribe = async () => {
      const initialBatteryLevel = await Battery.getBatteryLevelAsync();
      setBatteryLevel(initialBatteryLevel);

      const subscription = Battery.addBatteryLevelListener(({ batteryLevel }) => {
        setBatteryLevel(batteryLevel);
        console.log('batteryLevel changed!', batteryLevel);
      
        onBatteryLevelChange && onBatteryLevelChange(batteryLevel);
      });
      

      return () => {
        subscription.remove();
      };
    };

    _subscribe();
  }, []);

  const roundedBatteryLevel = batteryLevel !== null ? Math.floor(batteryLevel * 100) : null;
  updateBatterie(roundedBatteryLevel);
  return (
    null
  );
}

