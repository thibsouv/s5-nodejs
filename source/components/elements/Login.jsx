import React, { useState } from 'react';
import { Text, View, TextInput, StyleSheet } from 'react-native';

const LoginForm = ({ onPseudoChange, onEmailChange }) => {
  const [pseudo, setPseudo] = useState('');
  const [email, setEmail] = useState('');

  const handlePseudoChange = (text) => {
    setPseudo(text);
    onPseudoChange(text);
  };

  const handleEmailChange = (text) => {
    setEmail(text);
    onEmailChange(text);
  };

  return (
    <View>
      <Text style={styles.title}>Formulaire de connexion</Text>
      <View>
        <TextInput
          style={styles.text}
          placeholder="Entrez votre Pseudo"
          value={pseudo}
          onChangeText={handlePseudoChange}
        />
        <TextInput
          style={styles.text}
          placeholder="Entrez votre E-mail"
          value={email}
          onChangeText={handleEmailChange}
        />
      </View>
    </View>
  );
};

export default LoginForm;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 25,
      fontWeight: 'bold',
      textAlign: 'center',
      backgroundColor: 'rgba(255,255,255,1)',
      margin: 20,
      padding: 20,
      borderRadius: 10,
    },
    imageBack: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
    },
    text: {
      fontSize: 15,
      fontWeight: 'bold',
      textAlign: 'center',
      backgroundColor: 'rgba(255,255,255,0.8)',
      margin: 20,
      padding: 20,
      borderRadius: 10,
    },
    bouton: {
      backgroundColor: '#926740',
      color: '#fff',
      padding: 20,
      borderRadius: 10,
      margin: 40,
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 20,
      fontWeight: 'bold',
    },
  });
